Feature: A reCaptcha security protocol is implemented when registering as a new user
	When a new user attempts to register for an account, they must complete the reCaptcha
	If a bot attempts to register a new account, the reCaptcha stops them
	Everyone feels more secure knowing that only real people are using the system

Scenario: A new user attempts to creates an account but doesn't click the reCaptcha box
	Given They have correctly filled out the form
	And They don't click on the reCaptcha
	When They click register a new account
	Then The site redirects them back to account creation with a specific error message stating they need to complete the reCaptcha

Scenario: A new user attempts to create an account and successfully clicks on the reCaptcha box
	Given The user has filled out the form correctly
	And They have clicked on the reCaptcha box
	And The reCaptcha authenticates them as human
	When They click register their account
	Then They are redirected to the home page and their account is logged in and saved

Scenario: A new user attempts to create an account but needs to be verified by reCaptcha
	Given The user has filled out the form correctly
	And They click on reCaptcha
	When it requires more verification of them being a human
	Then the reCaptcha displays a series of images and a statement
	And the user must choose the correct images that coincide with the statement
	When they've chosen the correct images
	Then reCaptcha will verify them as human