Feature: Being able to filter out paid / unpaid content, to return only the type of resource desired.
	A series of radio buttons allows the user to choose free, paid, or both
	Filter out paid / unpaid content as desired

Scenario: A user searches for Math resources with the free radio button selected
	Given they've typed math in the search bar
	And they they've selected the free radio button
	When they click Go! to search
	Then their results will only include free resources

Scenario: A user searches for programming resources with the paid radio button selected
	Given they've typed programming in the search bar
	And they've selected the free radio button
	When they click Go! to search
	Then their search results will only include paid resources

Scenario: A user searches for art with the both radio button selected
	Given they've typed art in the search bar
	And they've selected the both radio button
	When they click Go! to search
	Then their search results will include both paid and unpaid resources

Scenario: A user searches for blacksmithing
	Given they haven't chosen a radio button
	When they click Go! to search
	Then they will receive paid and unpaid content as the default option