Feature: The most popular searches appear on the home page
	The 10 most popular queries from logged in users show up on the home page
	When a new query becomes popular, it appears the next time someone loads the page
	Each query can be clicked on for that user to then perform the same search

Scenario: A user visists Skillify when it has never been used before
	Given Our database has no logged queries in it
	When it tries to display the most popular queries
	Then it displays a message saying there are no popular searches yet

Scenario: A user visists Skillify after it's been used for a while
	Given our database has many searches from multiple users
	When the home page loads up
	Then the 10 most popular searches appear below the search bar

Scenario: A user visits Skillify when it's only been up a short while
	Given our database only has some data in it
	When the page loads up
	Then it will display as many popular queries as it can

Scenario: A user clicks on one of the popular queries
	Given there is a query to click on
	When the user clicks on it
	Then they will be redirected to the results of that specific query

Scenario: A user does a search for something becoming popular
	Given they return to the main page
	And that query has become popular enough to be on the front page
	When the home page loads up
	Then their query will appear in the most popular searches in its proper rank