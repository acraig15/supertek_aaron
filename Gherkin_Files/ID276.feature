Feature: Route all web errors to a pre-defined 404 page
	If the user navigates to a page that doesn't exist, show our 404
	If the user is directed to a page that doesn't exist, show our 404
	Every url error should route to our 404 page instead of the default asp.net page

Scenario: Type in non-existent URL
	Given the URL doesn't exist
	And the user tries to navigate to it
	When the server attempts to load the page
	Then it should redirect to our custom 404

Scenario: Click on a link or button that leads to an invalid page
	Given the page is invalid and can't be loaded
	When the page fails to load
	Then the user is redirected to our 404