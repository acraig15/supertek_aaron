﻿namespace UdemyCourseDetails
{
    public class RootObject
    {
        public string _class { get; set; }
        public int id { get; set; }
        public int num_subscribers { get; set; }
        public float rating { get; set; }
        public int num_reviews { get; set; }
        public int estimated_content_length { get; set; }
    }
}