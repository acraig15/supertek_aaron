namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProdPluralsightData")]
    public partial class ProdPluralsightData
    {
        [Key]
        [StringLength(255)]
        public string CourseId { get; set; }

        [Required]
        [StringLength(255)]
        public string CourseTitle { get; set; }

        public int DurationInSeconds { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReleaseDate { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        [StringLength(20)]
        public string AssessmentStatus { get; set; }

        [Required]
        [StringLength(20)]
        public string IsCourseRetired { get; set; }
    }
}
