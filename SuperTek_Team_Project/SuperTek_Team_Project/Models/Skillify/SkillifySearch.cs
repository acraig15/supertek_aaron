namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SuperTek_Team_Project.Skillify_Repository;

    public partial class SkillifySearch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SkillifySearch()
        {
            SkillifyResources = new HashSet<SkillifyResource>();
        }

        [Key]
        public int SearchID { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID { get; set; }

        [Required]
        [StringLength(255)]
        public string Query { get; set; }

        public DateTime Time { get; set; }

        [Required]
        [StringLength(32)]
        public string SkillLevel { get; set; }

        [Required]
        [StringLength(32)]
        public string Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SkillifyResource> SkillifyResources { get; set; }

        public virtual SkillifyUser SkillifyUser { get; set; }
    }
}
