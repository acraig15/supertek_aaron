namespace SuperTek_Team_Project.Models.Skillify
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApprovedCurriculum")]
    public partial class ApprovedCurriculum
    {
        public int ID { get; set; }

        [StringLength(128)]
        public string Title { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }

        public int? SkillifyCurriculums { get; set; }

        public virtual SkillifyCurriculum SkillifyCurriculum { get; set; }
    }
}
