namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SuperTek_Team_Project.Skillify_Repository;

    public partial class SkillifyUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SkillifyUser()
        {
            SkillifyCurriculums = new HashSet<SkillifyCurriculum>();
            SkillifySearches = new HashSet<SkillifySearch>();
        }

        [Key]
        public string UserID { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string Username { get; set; }

        public int? Age { get; set; }

        public DateTime? Birthday { get; set; }

        [StringLength(128)]
        public string ASPIdentityID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SkillifyCurriculum> SkillifyCurriculums { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SkillifySearch> SkillifySearches { get; set; }
    }
}
