namespace SuperTek_Team_Project.Models.Skillify
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CurToAppCur")]
    public partial class CurToAppCur
    {
        public int ID { get; set; }

        public int? AppCurID { get; set; }

        public int? CurID { get; set; }
    }
}
