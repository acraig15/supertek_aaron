namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ResourceToCurriculum")]
    public partial class ResourceToCurriculum
    {
        public int ID { get; set; }

        public int? CurriculumID { get; set; }

        public int? ResourceID { get; set; }
    }
}
