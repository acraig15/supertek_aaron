﻿using System;
using System.Collections.Generic;

namespace Udemy_Models
{
    public class VisibleInstructor
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string display_name { get; set; }
        public string job_title { get; set; }
        public string image_50x50 { get; set; }
        public string image_100x100 { get; set; }
        public string initials { get; set; }
        public string url { get; set; }
    }

    public class Price
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class ListPrice
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class SavingPrice
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class Buyable
    {
        public int id { get; set; }
        public string type { get; set; }
    }

    public class Campaign
    {
        public string code { get; set; }
        public DateTime end_time { get; set; }
        public bool is_instructor_created { get; set; }
        public bool is_public { get; set; }
        public DateTime start_time { get; set; }
        public string campaign_type { get; set; }
        public int uses_remaining { get; set; }
        public int maximum_uses { get; set; }
    }

    public class Rate
    {
        public string type { get; set; }
        public int value { get; set; }
    }

    public class Discount
    {
        public Price price { get; set; }
        public ListPrice list_price { get; set; }
        public SavingPrice saving_price { get; set; }
        public int discount_percent_for_display { get; set; }
        public Buyable buyable { get; set; }
        public Campaign campaign { get; set; }
        public Rate rate { get; set; }
    }

    public class DiscountPrice
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class RatingDistribution
    {
        public int count { get; set; }
        public int rating { get; set; }
    }

    public class Features
    {
        public string _class { get; set; }
        public bool discussions_create { get; set; }
        public bool discussions_view { get; set; }
        public bool discussions_replies_create { get; set; }
        public bool enroll { get; set; }
        public bool reviews_create { get; set; }
        public bool reviews_view { get; set; }
        public bool reviews_responses_create { get; set; }
        public bool announcements_comments_view { get; set; }
        public bool educational_announcements_create { get; set; }
        public bool promotional_announcements_create { get; set; }
        public bool promotions_create { get; set; }
        public bool promotions_view { get; set; }
        public bool students_view { get; set; }
    }

    public class NotificationSettings
    {
    }

    public class PrimaryCategory
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string title { get; set; }
    }

    public class PrimarySubcategory
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string title { get; set; }
    }

    public class Locale
    {
        public string _class { get; set; }
        public string locale { get; set; }
        public string title { get; set; }
        public string english_title { get; set; }
    }

    public class RequirementsData
    {
        public List<object> items { get; set; }
    }

    public class WhatYouWillLearnData
    {
        public List<string> items { get; set; }
    }

    public class WhoShouldAttendData
    {
        public List<object> items { get; set; }
    }

    public class Source
    {
        public string video_codec { get; set; }
        public string format { get; set; }
        public string audio_bitrate { get; set; }
        public string height { get; set; }
        public string duration { get; set; }
        public string filesize { get; set; }
        public string audio_sample_rate { get; set; }
        public int bitrate_int { get; set; }
        public string audio_codec { get; set; }
        public string audio_duration { get; set; }
        public string bitrate { get; set; }
        public string display_aspect_ratio { get; set; }
        public string audio_channels { get; set; }
        public string width { get; set; }
        public string size { get; set; }
        public string frame_rate { get; set; }
        public string pixel_aspect_ratio { get; set; }
        public string video_duration { get; set; }
        public string video_bitrate { get; set; }
    }

    public class __invalid_type__480
    {
        public string video_codec { get; set; }
        public int height { get; set; }
        public object total_bitrate_in_kbps { get; set; }
        public object md5_checksum { get; set; }
        public string audio_sample_rate { get; set; }
        public object channels { get; set; }
        public string state { get; set; }
        public string url { get; set; }
        public object fragment_duration_in_ms { get; set; }
        public object video_bitrate_in_kbps { get; set; }
        public string id { get; set; }
        public object rfc_6381_audio_codec { get; set; }
        public string format { get; set; }
        public string audio_codec { get; set; }
        public string file_size_in_bytes { get; set; }
        public string duration_in_ms { get; set; }
        public int audio_bitrate_in_kbps { get; set; }
        public string frame_rate { get; set; }
        public int width { get; set; }
        public int label { get; set; }
        public string type { get; set; }
        public object name { get; set; }
        public object rfc_6381_video_codec { get; set; }
    }

    public class __invalid_type__144
    {
        public string video_codec { get; set; }
        public int height { get; set; }
        public object total_bitrate_in_kbps { get; set; }
        public object md5_checksum { get; set; }
        public string audio_sample_rate { get; set; }
        public object channels { get; set; }
        public string state { get; set; }
        public string url { get; set; }
        public object fragment_duration_in_ms { get; set; }
        public object video_bitrate_in_kbps { get; set; }
        public string id { get; set; }
        public object rfc_6381_audio_codec { get; set; }
        public string format { get; set; }
        public string audio_codec { get; set; }
        public string file_size_in_bytes { get; set; }
        public string duration_in_ms { get; set; }
        public int audio_bitrate_in_kbps { get; set; }
        public string frame_rate { get; set; }
        public int width { get; set; }
        public int label { get; set; }
        public string type { get; set; }
        public object name { get; set; }
        public object rfc_6381_video_codec { get; set; }
    }

    public class __invalid_type__720
    {
        public string video_codec { get; set; }
        public int height { get; set; }
        public object total_bitrate_in_kbps { get; set; }
        public object md5_checksum { get; set; }
        public string audio_sample_rate { get; set; }
        public object channels { get; set; }
        public string state { get; set; }
        public string url { get; set; }
        public object fragment_duration_in_ms { get; set; }
        public object video_bitrate_in_kbps { get; set; }
        public string id { get; set; }
        public object rfc_6381_audio_codec { get; set; }
        public string format { get; set; }
        public string audio_codec { get; set; }
        public string file_size_in_bytes { get; set; }
        public string duration_in_ms { get; set; }
        public int audio_bitrate_in_kbps { get; set; }
        public string frame_rate { get; set; }
        public int width { get; set; }
        public int label { get; set; }
        public string type { get; set; }
        public object name { get; set; }
        public object rfc_6381_video_codec { get; set; }
    }

    public class __invalid_type__360
    {
        public string video_codec { get; set; }
        public int height { get; set; }
        public object total_bitrate_in_kbps { get; set; }
        public object md5_checksum { get; set; }
        public string audio_sample_rate { get; set; }
        public object channels { get; set; }
        public string state { get; set; }
        public string url { get; set; }
        public object fragment_duration_in_ms { get; set; }
        public object video_bitrate_in_kbps { get; set; }
        public string id { get; set; }
        public object rfc_6381_audio_codec { get; set; }
        public string format { get; set; }
        public string audio_codec { get; set; }
        public string file_size_in_bytes { get; set; }
        public string duration_in_ms { get; set; }
        public int audio_bitrate_in_kbps { get; set; }
        public string frame_rate { get; set; }
        public int width { get; set; }
        public int label { get; set; }
        public string type { get; set; }
        public object name { get; set; }
        public object rfc_6381_video_codec { get; set; }
    }

    public class Outputs
    {
        public __invalid_type__480 __invalid_name__480 { get; set; }
        public __invalid_type__144 __invalid_name__144 { get; set; }
        public __invalid_type__720 __invalid_name__720 { get; set; }
        public __invalid_type__360 __invalid_name__360 { get; set; }
    }

    public class ThumbnailsData
    {
        public int num_of_thumbnails { get; set; }
        public string interval { get; set; }
        public string file_extension { get; set; }
    }

    public class Dimensions
    {
        public object height { get; set; }
        public object width { get; set; }
    }

    public class Data
    {
        public string encoding_com_job_id { get; set; }
        public string upload_bucket { get; set; }
        public Source source { get; set; }
        public string ticket_id { get; set; }
        public object isHD { get; set; }
        public Outputs outputs { get; set; }
        public string domain { get; set; }
        public ThumbnailsData thumbnails_data { get; set; }
        public string ticketId { get; set; }
        public string fileSize { get; set; }
        public string original_name { get; set; }
        public Dimensions dimensions { get; set; }
        public string storage_bucket { get; set; }
        public string name { get; set; }
        public object duration { get; set; }
        public string locale { get; set; }
        public int? inputHeight { get; set; }
        public int? speechmatics_job_id { get; set; }
        public int? jobId { get; set; }
    }

    public class Video
    {
        public string type { get; set; }
        public string label { get; set; }
        public string file { get; set; }
    }

    public class DownloadUrls
    {
        public List<Video> Video { get; set; }
    }

    public class PromoAsset
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string asset_type { get; set; }
        public string title { get; set; }
        public string title_cleaned { get; set; }
        public string description { get; set; }
        public int length { get; set; }
        public int status { get; set; }
        public Data data { get; set; }
        public DownloadUrls download_urls { get; set; }
    }

    public class Faq
    {
        public string answer { get; set; }
        public string question { get; set; }
    }

    public class PriceDetail
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class GoogleInAppPriceDetail
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class AppleInAppPriceDetail
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string price_string { get; set; }
        public string currency_symbol { get; set; }
    }

    public class ClientSettings
    {
        public bool early_access { get; set; }
        public bool lecture_export_enabled { get; set; }
        public bool coding_exercises_enabled { get; set; }
        public bool machine_cc_enabled { get; set; }
    }

    public class QualityReviewProcess
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string status { get; set; }
        public double? score { get; set; }
        public int? admin_rating { get; set; }
    }

    public class Label
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string display_name { get; set; }
    }

    public class CourseHasLabel
    {
        public string _class { get; set; }
        public int id { get; set; }
        public Label label { get; set; }
        public bool is_primary { get; set; }
    }

    public class Category
    {
        public string title { get; set; }
        public string tracking_object_type { get; set; }
        public int id { get; set; }
        public string url { get; set; }
    }

    public class Label2
    {
        public string display_name { get; set; }
        public int id { get; set; }
        public string topic_channel_url { get; set; }
        public string tracking_object_type { get; set; }
    }

    public class ContextInfo
    {
        public Category category { get; set; }
        public object subcategory { get; set; }
        public Label2 label { get; set; }
    }

    public class BestsellerBadgeContent
    {
        public string _class { get; set; }
        public string id { get; set; }
        public string badge_text { get; set; }
        public string badge_family { get; set; }
        public ContextInfo context_info { get; set; }
    }

    public class Category2
    {
        public string title { get; set; }
        public string tracking_object_type { get; set; }
        public int id { get; set; }
        public string url { get; set; }
    }

    public class Label3
    {
        public string display_name { get; set; }
        public int id { get; set; }
        public string topic_channel_url { get; set; }
        public string tracking_object_type { get; set; }
    }

    public class ContextInfo2
    {
        public Category2 category { get; set; }
        public object subcategory { get; set; }
        public Label3 label { get; set; }
    }

    public class Result
    {
        public string _class { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public bool is_paid { get; set; }
        public string price { get; set; }
        public List<VisibleInstructor> visible_instructors { get; set; }
        public string image_125_H { get; set; }
        public string image_240x135 { get; set; }
        public bool is_practice_test_course { get; set; }
        public string image_480x270 { get; set; }
        public string published_title { get; set; }
        public string description { get; set; }
        public string headline { get; set; }
        public int num_subscribers { get; set; }
        public Discount discount { get; set; }
        public DiscountPrice discount_price { get; set; }
        public double avg_rating { get; set; }
        public double avg_rating_recent { get; set; }
        public double rating { get; set; }
        public int num_reviews { get; set; }
        public int num_reviews_recent { get; set; }
        public List<RatingDistribution> rating_distribution { get; set; }
        public string search_status { get; set; }
        public object favorite_time { get; set; }
        public object archive_time { get; set; }
        public string earnings { get; set; }
        public int completion_ratio { get; set; }
        public bool is_wishlisted { get; set; }
        public int num_quizzes { get; set; }
        public int num_lectures { get; set; }
        public int num_published_lectures { get; set; }
        public int num_published_quizzes { get; set; }
        public int num_curriculum_items { get; set; }
        public bool is_private { get; set; }
        public int num_practice_tests { get; set; }
        public int num_published_practice_tests { get; set; }
        public string original_price_text { get; set; }
        public string quality_status { get; set; }
        public string status_label { get; set; }
        public bool can_edit { get; set; }
        public Features features { get; set; }
        public string gift_url { get; set; }
        public int num_invitation_requests { get; set; }
        public NotificationSettings notification_settings { get; set; }
        public bool is_banned { get; set; }
        public bool is_published { get; set; }
        public object intended_category { get; set; }
        public string image_48x27 { get; set; }
        public string image_50x50 { get; set; }
        public string image_75x75 { get; set; }
        public string image_96x54 { get; set; }
        public string image_100x100 { get; set; }
        public string image_200_H { get; set; }
        public string image_304x171 { get; set; }
        public string image_750x422 { get; set; }
        public bool has_certificate { get; set; }
        public PrimaryCategory primary_category { get; set; }
        public PrimarySubcategory primary_subcategory { get; set; }
        public bool is_in_content_subscription { get; set; }
        public Locale locale { get; set; }
        public bool has_closed_caption { get; set; }
        public List<string> caption_languages { get; set; }
        public DateTime created { get; set; }
        public string instructional_level { get; set; }
        public string instructional_level_simple { get; set; }
        public int estimated_content_length { get; set; }
        public string content_info { get; set; }
        public int content_length_practice_test_questions { get; set; }
        public RequirementsData requirements_data { get; set; }
        public WhatYouWillLearnData what_you_will_learn_data { get; set; }
        public WhoShouldAttendData who_should_attend_data { get; set; }
        public bool is_available_on_google_app { get; set; }
        public object organization_id { get; set; }
        public string google_in_app_purchase_price_text { get; set; }
        public PromoAsset promo_asset { get; set; }
        public bool is_user_subscribed { get; set; }
        public string apple_in_app_product_id { get; set; }
        public bool is_available_on_ios { get; set; }
        public string google_in_app_product_id { get; set; }
        public List<Faq> faq { get; set; }
        public string apple_in_app_purchase_price_text { get; set; }
        public string type_label { get; set; }
        public PriceDetail price_detail { get; set; }
        public GoogleInAppPriceDetail google_in_app_price_detail { get; set; }
        public AppleInAppPriceDetail apple_in_app_price_detail { get; set; }
        public ClientSettings client_settings { get; set; }
        public QualityReviewProcess quality_review_process { get; set; }
        public bool is_organization_only { get; set; }
        public string buyable_object_type { get; set; }
        public DateTime published_time { get; set; }
        public bool is_marketing_boost_agreed { get; set; }
        public bool is_taking_disabled { get; set; }
        public int content_length_video { get; set; }
        public string checkout_url { get; set; }
        public List<object> prerequisites { get; set; }
        public List<string> objectives { get; set; }
        public List<string> objectives_summary { get; set; }
        public List<object> target_audiences { get; set; }
        public object last_accessed_time { get; set; }
        public object enrollment_time { get; set; }
        public List<CourseHasLabel> course_has_labels { get; set; }
        public bool has_multi_rep { get; set; }
        public BestsellerBadgeContent bestseller_badge_content { get; set; }
        public List<object> badges { get; set; }
        public object free_course_subscribe_url { get; set; }
        public bool is_recently_published { get; set; }
        public string last_update_date { get; set; }
        public int num_article_assets { get; set; }
        public int num_coding_exercises { get; set; }
        public int num_assignments { get; set; }
        public int num_additional_assets { get; set; }
        public string preview_url { get; set; }
        public string landing_preview_as_guest_url { get; set; }
        public ContextInfo2 context_info { get; set; }
        public double predictive_score { get; set; }
        public double relevancy_score { get; set; }
        public object input_features { get; set; }
        public object lecture_search_result { get; set; }
    }

    public class Option
    {
        public string title { get; set; }
        public string key { get; set; }
        public int count { get; set; }
        public string value { get; set; }
    }

    public class Aggregation
    {
        public string title { get; set; }
        public List<Option> options { get; set; }
        public string id { get; set; }
    }

    public class Suggestion
    {
        public object phrase { get; set; }
    }

    public class RootObject
    {
        public int count { get; set; }
        public string next { get; set; }
        public object previous { get; set; }
        public List<Result> results { get; set; }
        public List<Aggregation> aggregations { get; set; }
        public Suggestion suggestion { get; set; }
    }
}