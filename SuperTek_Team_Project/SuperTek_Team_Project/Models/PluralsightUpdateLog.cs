namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PluralsightUpdateLog")]
    public partial class PluralsightUpdateLog
    {
        public int ID { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }
    }
}
