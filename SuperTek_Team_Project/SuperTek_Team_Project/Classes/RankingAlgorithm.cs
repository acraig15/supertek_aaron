﻿using System;
using System.Collections.Generic;
using System.Linq;
using SuperTek_Team_Project.ExternalAPIRequests;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.Classes
{
    public class RankingAlgorithm
    {
      
        private static Random random = new Random();
        /// <summary>
        /// Rank the given video according to the Skillify Algorithm.
        /// </summary>
        /// <param name="Video">The SearchResult containing the video to be ranked.</param>
        /// <returns>The rating as an integer.</returns>
        public static float RankVideo(SuperTek_Team_Project.ViewModels.SearchResult Video)
        {
            //Rank the video if it's from YouTube
            if (Video.Source == "YouTube")
            {
                YouTubeVideoStats.RootObject data;
                //Get first video of playlist, if it's a playlist
                if (Video.Type == "Playlist")
                {
                    var theRightVideoID = YouTubeAPI.GetFirstVideoInPlaylist(Video.VideoID);
                    data = YouTubeAPI.VideoStats(theRightVideoID.resourceId.videoId);
                }
                else
                {
                    data = YouTubeAPI.VideoStats(Video.VideoID);
                }
                //Get the likes
                float.TryParse(data.items[0].statistics.likeCount, out float likes);
                //Get the dislikes
                float.TryParse(data.items[0].statistics.dislikeCount, out float disLikes);
                //Get the views
                float.TryParse(data.items[0].statistics.viewCount, out float viewCount);
                //Get favorites
                float.TryParse(data.items[0].statistics.favoriteCount, out float favoriteCount);

                //Math it up
                float likesVsDisLikes = (likes) - (disLikes * 1.5f); //Likes are important, shows engagement
                float viewership = viewCount * .075f; //Give the amount of views a very low part to play
                float favorites = favoriteCount * 50; //Favorites should be rewarded

                return likesVsDisLikes + viewership + favorites;
            }
            //Rank the video if it's from Udemy
            else if(Video.Source == "Udemy")
            {
                int.TryParse(Video.VideoID, out int UdemyID); //Get the Udemy id as an integer
                var data = UdemyAPI.GetCourseDetails(UdemyID);

                float CourseRating = data.rating * 100; //Give the rating the most powerful part of the score
                float RatingAndReviews = data.rating * data.num_reviews; //If the course has a good rating, then this will be high
                float SubscriberCount = data.num_subscribers * .5f; //Limit it a little bit
                float CourseLength = data.estimated_content_length * 25; //The length of the course gives it a higher rating

                return CourseRating + RatingAndReviews + SubscriberCount + CourseLength;
            }
            //Rank the Khan Academy Videos here
            else if(Video.Source == "Khan Academy") //Be sure you've set the source of your videos when you return them
            {

                //algorithm for khan academy resources
                //found average rating based on an SQL query on resources table 
                //took a note from Luis's book and grabbed the next random double 
                return (float)88340.3 + (float)(random.NextDouble() * 15000);
            }
            // Rank the Pluralsight Videos here
            // Pluralisgh
            else if (Video.Source == "Pluralsight")
            {                
                // For Pluralsight ranking, this part of the algorithm returns a median value based on sample
                // data from the Youtube and Udemy video ranking data and randomized
                // to mix the data with other classes.
                return (float)72109.8 + (float)(random.NextDouble()* 20000);
            }

            return 0;
        }
        /// <summary>
        /// Sorts a list of SearchResults based on their ranking.
        /// </summary>
        /// <param name="VideoList">The list of videos to be sorted.</param>
        /// <returns>A new IList of SearchResults, sorted. </returns>
        public static IList<SearchResult> SortResults(IList<SearchResult> VideoList)
        {
            //Organize them by their rating
            IEnumerable<SearchResult> list = VideoList.OrderByDescending(SearchResult => SearchResult.SkillifyRating);

            //Give all the non rated resources the average so they appear in the middle of the list
            
            //if the list is empty, do not get an average
            float average = list.Count();
            if (list.Count() > 0)
            {
               average = list.Average(e => e.SkillifyRating);
            }
            
            foreach(var item in list)
            {
                if (item.SkillifyRating == 0)
                {
                    item.SkillifyRating = average;
                }
            }

            return list.ToList();
        }
    }
}