﻿namespace SuperTek_Team_Project.ViewModels
{
    public class SearchResult
    {
        public int ResourceID { get; set; }
        public string Title { get; set; } //The title of the video/playlist
        public string Description { get; set; } //Description of video/playlist
        public string Image { get; set; } //The final image to display
        public string Url { get; set; } //Finished, clickable URL
        public string SkillLevel { get; set; } //skill level associated with video/playlist
        public string Type { get; set; } //What kind of resource it is, such as Video, Exercise, Playlist, or Course
        public string Source { get; set; } //Where the video comes from, YouTube, Udemy, etc..
        public string VideoID { get; set; } //Identifiable id for the specific video
        public float SkillifyRating { get; set; } //The rating we give the video
        public float ResourceLength { get; set; } //The legnth of the playlist, course, video, or estimated length of an exercise
    }
}