﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.Models;

namespace SuperTek_Team_Project.ViewModels
{
    public class SearchHistoryModel
    {
        //reference to the resource added
        public int ResourceID { get; set; }
        //search made by the user 
        public string search { get; set;}
        //date the search was conducted
        public DateTime date { get; set; }
        //title of the resource returned
        public string title { get; set; }
        //description of the resource returned
        public string description { get; set; }
        //url of the resource returned 
        public string url { get; set; }
    }
}