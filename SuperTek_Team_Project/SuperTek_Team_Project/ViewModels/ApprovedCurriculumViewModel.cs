﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.Models;

namespace SuperTek_Team_Project.ViewModels
{
    public class ApprovedCurriculumViewModel
    {
        public string Title { get; set; }
        public List<String> CurriculumTitles { get; set; }
    }
}