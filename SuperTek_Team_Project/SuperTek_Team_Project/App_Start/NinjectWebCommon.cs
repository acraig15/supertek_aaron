﻿using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using SuperTek_Team_Project.App_Start;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.DAL.Skillify_Repository.Skillify_Interfaces;
using SuperTek_Team_Project.Skillify_Repository;
using System;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SuperTek_Team_Project.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(SuperTek_Team_Project.App_Start.NinjectWebCommon), "Stop")]

namespace SuperTek_Team_Project.App_Start
{
     public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<ISearchRepository>().To<SearchRepository>();
            kernel.Bind<ICurriculumRepository>().To<CurriculumRepository>();
            kernel.Bind<IResourceRepository>().To<ResourceRepository>();
            kernel.Bind<IResToCurRepository>().To<ResToCurRepository>();
            kernel.Bind<IApprovedCurriculumRepository>().To<ApprovedCurriculumRepository>();
            kernel.Bind<ICurToAppCurRepository>().To<CurToAppCurRepository>();
        }
    }
}