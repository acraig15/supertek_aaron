﻿
/*using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Skillify_Repository;

namespace SuperTek_Team_Project.App_Start
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<ISearchRepository>().To<SearchRepository>();
            kernel.Bind<ICurriculumRepository>().To<CurriculumRepository>();
            kernel.Bind<IResourceRepository>().To<ResourceRepository>();
        }
    }
}*/