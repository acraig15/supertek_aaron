﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SuperTek_Team_Project.ViewModels;
using SuperTek_Team_Project.Models;
using System.Net;
using System.Data.Entity;
using System.Data;
using Microsoft.AspNet.Identity;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models.Skillify;

namespace SuperTek_Team_Project.Controllers
{
    public class UserPortalController : Controller
    {
        //private SkillifyContext db = new SkillifyContext();

       
        private UnitOfWork unitOfWork = new UnitOfWork(new SkillifyContext());
        
      
        // GET: UserPortal/SearchHistory
        public ActionResult SearchHistory()
        {
            //get logged in user's ID 
            var aspID = User.Identity.GetUserId();

            List<SearchHistoryModel> history = new List<SearchHistoryModel>();

            history = unitOfWork.Curriculum.SearchHistory(aspID);

            return View(history);
        }

        //view all curriculums from a given user
        public ActionResult Curriculum()
        {
       
            var aspID = User.Identity.GetUserId();

            List<SkillifyCurriculum> list = new List<SkillifyCurriculum>();
            list =  unitOfWork.Curriculum.CurrToUser(aspID);
                            
            return View(list);
        }

        //add course to a user's curriculum
        //if a user is not logged in, it asks for either registration or login.
        public ActionResult AddResource(int? id)
        {
            var aspID = User.Identity.GetUserId();

            //if the user is not logged in, display a messsage and send to Login view 
            if (aspID == null)
            {
                return RedirectToAction("CurrLogin", "Account", new { area = "" });               
            }

            List<SkillifyCurriculum> cur = new List<SkillifyCurriculum>();

            var userCur = unitOfWork.Curriculum.CurrToUser(aspID);
            var appCur = unitOfWork.AppCur.AppCurToUser(aspID);
            
            foreach (var item in userCur)
            {
                SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(item.CurriculumID);

              
                SkillifyCurriculum curriculum = new SkillifyCurriculum()
                {
                    Title = item.Title,
                    SkillifyResources = id,
                    CurriculumID = skillifyCurriculum.CurriculumID
                };
                cur.Add(curriculum);

             
            }
            foreach (var item in appCur)
            {
                SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(item.CurriculumID);


                SkillifyCurriculum curriculum = new SkillifyCurriculum()
                {
                    Title = item.Title,
                    SkillifyResources = id,
                    CurriculumID = skillifyCurriculum.CurriculumID
                };
                cur.Add(curriculum);


            }

            return View(cur);
        }

        //action result to add a resource to a user's curriculum
        public ActionResult AddResourceToCur(int id, int res)
        {
            //get the resources that match the resource id that was sent to this action result
            SkillifyResource newRes = new SkillifyResource();
            newRes = unitOfWork.Resource.Get(res);

            //find the curriculum that has the same curriculum id that was sent to page
            SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(id);

            //add the resource to curriculum connection to the bridge table
            
            
                ResourceToCurriculum resource = new ResourceToCurriculum()
                {
                    ResourceID = newRes.ResourceID,
                    CurriculumID = skillifyCurriculum.CurriculumID
                };



                //adding the resource to the the list of resources for the specific curriculum
                //also saving it to the database
                skillifyCurriculum.SkillifyResources = newRes.ResourceID;


                //add to resToCur repo
                unitOfWork.ResToCur.Add(resource);
                unitOfWork.Save();

                //add to database 
                //db.ResourceToCurriculums.Add(resource);
                //db.SaveChanges();

                //db.Entry(skillifyCurriculum).State = EntityState.Modified;
                unitOfWork.Curriculum.Update(skillifyCurriculum);
                unitOfWork.Save();
                //db.SaveChanges();

            //if it is approved, redirect to user approved curriculums
                if(skillifyCurriculum.Approved != null)
            {
                return RedirectToAction("UserApprovedCurriculum");
            }                    
            //redirect to custom curriculum page after a resource is added 
            return RedirectToAction("Curriculum");

        }

        public ActionResult AddCurriculum()
        {           
            return View();
        }

        [HttpPost]
        public ActionResult AddCurriculum(string Title, bool IsPublic)
            {
            var aspID = User.Identity.GetUserId();

            SkillifyCurriculum curriculum = new SkillifyCurriculum()
            {
                DateCreated = DateTime.Now,
                SkillifyUserID = aspID,
                IsPublic = IsPublic,
                Title = Title
            };

            //add to curr repo
            unitOfWork.Curriculum.Add(curriculum);
            unitOfWork.Save();

            //add curriculum to database
            //db.SkillifyCurriculums.Add(curriculum);
            //db.SaveChanges();
            return RedirectToAction("Curriculum");
             
            }
        public ActionResult EditCurriculum(int id)
        {
     
            SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(id);

            if (skillifyCurriculum == null)
            {
                return HttpNotFound();
            }
            return View(skillifyCurriculum);

        }

        [HttpPost]
        public ActionResult EditCurriculum(int id, string Title, DateTime? DateFinished, bool IsCreatorFavorite, bool IsPublic, int? UsersRating)
        {
            SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(id);
            skillifyCurriculum.Title = Title;
            skillifyCurriculum.DateFinished = DateFinished;
            skillifyCurriculum.IsCreatorFavorite = IsCreatorFavorite;
            skillifyCurriculum.IsPublic = IsPublic;
            skillifyCurriculum.UsersRating = UsersRating;


            //db.Entry(skillifyCurriculum).State = EntityState.Modified;
            unitOfWork.Curriculum.Update(skillifyCurriculum);
            unitOfWork.Save();
            //db.SaveChanges();
            return View("EditCurriculum");
            }

        // GET: SkillifyCurriculums/DeleteCurriculum
        public ActionResult DeleteCurriculum(int id)
        {
           
            SkillifyCurriculum skillifyCurriculum = unitOfWork.Curriculum.Get(id);
            if (skillifyCurriculum == null)
            {
                return HttpNotFound();
            }
            return View(skillifyCurriculum);
        }

        // POST: SkillifyCurriculums/DeleteCurriculum
        [HttpPost, ActionName("DeleteCurriculum")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SkillifyCurriculum cur = unitOfWork.Curriculum.Get(id);

            unitOfWork.Curriculum.Delete(cur);
            unitOfWork.Save();
          
            //db.SkillifyCurriculums.Remove(skillifyCurriculum);
            //db.SaveChanges();
            return RedirectToAction("Curriculum");
        }

        // GET: SkillifyResources/Edit/5
        public ActionResult EditResource(int id)
        {
            
            SkillifyResource skillifyResource = unitOfWork.Resource.Get(id);
            if (skillifyResource == null)
            {
                return HttpNotFound();
            }
            return View(skillifyResource);
        }

        [HttpPost]
        public ActionResult EditResource(int id, bool IsDone, int UserRating)
        {
            SkillifyResource skillifyResource = unitOfWork.Resource.Get(id);
            skillifyResource.IsDone = IsDone;
            skillifyResource.UserRating = UserRating;

            //db.Entry(skillifyResource).State = EntityState.Modified;
            //db.SaveChanges();

            unitOfWork.Resource.Update(skillifyResource);
            unitOfWork.Save();
            return View("EditResource");
        }

        //see the list of resources in a curriculum based on curriculum ID that was sent to the action result
        public ActionResult ResourcesView(int? id)
        {
            List<SkillifyResource> resources = new List<SkillifyResource>();
            resources = unitOfWork.Resource.ResToCurList(id);

            return View(resources);
        }

        public ActionResult AddUserApprovedCurriculum()
        {
            var aspID = User.Identity.GetUserId();

            List<SkillifyCurriculum> list = new List<SkillifyCurriculum>();

            list = unitOfWork.AppCur.ListToAdd(aspID);
           
            return RedirectToAction("UserApprovedCurriculum");
        }

        public ActionResult UserApprovedCurriculum()
        {
            List<SkillifyCurriculum> list = new List<SkillifyCurriculum>();

            var aspID = User.Identity.GetUserId();

            list = unitOfWork.AppCur.AppCurToUser(aspID);

            return View(list);
        }

        public ActionResult DeleteApprovedCur()
        {
            var aspID = User.Identity.GetUserId();

            unitOfWork.AppCur.DeleteUserAppCur(aspID);
;

            return RedirectToAction("UserApprovedCurriculum");
        }
    }
}
    
    
    

