﻿using System.Collections.Generic;
using System.Web.Mvc;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.ViewModels;
using System;
using Microsoft.AspNet.Identity;
using SuperTek_Team_Project.ExternalAPIRequests;
using System.Diagnostics;
using SuperTek_Team_Project.Classes;
using System.Net;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models.Skillify;
using Ninject;
using System.Reflection;


namespace SuperTek_Team_Project.Controllers
{
    public class HomeController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork(new SkillifyContext());

        SkillifyContext db = new SkillifyContext(); //We still need to get rid of this

        [RequireHttps]
        public ActionResult Index()
        {
            //Retrieve the most recent searches
            return View(unitOfWork.Search.GetMostPopularSearches(12));
        }

        public ActionResult ApprovedCurriculums()

        {
            ApprovedCurriculumViewModel app = new ApprovedCurriculumViewModel();

            app = unitOfWork.AppCur.GetCSAppCurList();

            return View(app);
        }

        [RequireHttps]
        public ActionResult SearchResults(string query = "", string level = "", string cost = "")
        {
            //Grab the aspects of the query string that is needed
            query = Request.QueryString["query"];
            level = Request.QueryString["level"];
            cost = Request.QueryString["cost"];

            //Encode to make it safe to search
            query = Server.UrlEncode(query);

            //Check if it's a valid search
            if (query == null)
                return View("~/Views/Home/Index.cshtml");
            //Then run the search if it is
            else
            {
                //Convert the serach results into one file to return
                List<SearchResult> finalList = new List<SearchResult>();
                if (cost == "both")
                {
                    try // to get the results from Udemy
                    {
                        //Add results from Udemy to final list
                        finalList.AddRange(UdemyAPI.CourseListSearch(query, level, cost));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get the results from YouTube
                    {
                        //call Youtube API class
                        finalList.AddRange(YouTubeAPI.Search(query, level));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get the results from Khan Academy
                    {
                        //call Khan Academy API class
                        finalList.AddRange(KhanAPI.KhanAcad(query, level));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get search results from Plurasight
                    {
                        IList<SearchResult> pluralResults = Pluralsight.Search(query, level);
                        foreach (SearchResult s in pluralResults)
                        {
                            finalList.Add(s);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
                else if(cost == "paid")
                {
                    try // to get the results from Udemy
                    {
                        //Add results from Udemy to final list
                        finalList.AddRange(UdemyAPI.CourseListSearch(query, level, cost));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get search results from Plurasight
                    {
                        IList<SearchResult> pluralResults = Pluralsight.Search(query, level);
                        foreach (SearchResult s in pluralResults)
                        {
                            finalList.Add(s);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
                else if(cost == "free")
                {
                    try // to get the results from Khan Academy
                    {
                        //call Khan Academy API class
                        finalList.AddRange(KhanAPI.KhanAcad(query, level));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get the results from Udemy
                    {
                        //Add results from Udemy to final list
                        finalList.AddRange(UdemyAPI.CourseListSearch(query, level, cost));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    try // to get the results from YouTube
                    {
                        //call Youtube API class
                        finalList.AddRange(YouTubeAPI.Search(query, level));
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
                //Give the videos ratings
                foreach (var item in finalList)
                {
                    item.SkillifyRating = Classes.RankingAlgorithm.RankVideo(item);
                }

                //Decode the url back to normal readable
                query = Server.UrlDecode(query);

                //save searches and results if the user is logged in
                if (User.Identity.IsAuthenticated)
                {
                    SkillifySearch search = new SkillifySearch
                    {
                        Query = query,
                        Time = DateTime.Now,
                        SkillLevel = level,
                        Type = "Skill"
                    };

                    var aspID = User.Identity.GetUserId();
                    search.UserID = unitOfWork.User.GetUserByIdentityID(aspID).UserID;

                    //saving searches to database
                    unitOfWork.Search.Add(search);

                    //db.SkillifySearches.Add(search);
                    //db.SaveChanges();

                    foreach (SearchResult item in finalList)
                    {

                        SkillifyResource result = new SkillifyResource
                        {
                            SkillifyRating = item.SkillifyRating,
                            Description = item.Description,
                            IsDone = false,
                            isPaid = true,
                            Photo = item.Image,
                            Source = item.Source,
                            SkillLevel = item.SkillLevel,
                            Title = item.Title,
                            Url = item.Url,
                            Type = item.Type,
                            SearchID = search.SearchID
                        };

                        //saving results to database
                        //db.SkillifyResources.Add(result);
                        //db.SaveChanges();
                        unitOfWork.Resource.Add(result);
                        unitOfWork.Save();

                        item.ResourceID = result.ResourceID;
                    }
                }
                else //Still save the query
                {
                    SkillifySearch search = new SkillifySearch
                    {
                        Query = query,
                        Time = DateTime.Now,
                        SkillLevel = level,
                        Type = "Skill",

                        //default if the user is not logged in 
                        UserID = "Not Logged In"
                    };

                    unitOfWork.Search.Add(search);
                    unitOfWork.Save();
                }

                //add the search string to ViewBag in case no results are found so
                //search term can be presented to end-user
                ViewBag.queryString = query;

                return View(RankingAlgorithm.SortResults(finalList));

            }
        }

        /// <summary>
        /// Updates the Pluralsight course database table with current courses from
        /// a csv file downloaded from pluralsight url: https://api.pluralsight.com/api-v0.9/courses
        /// </summary>

        [Authorize(Roles = "Admin")]
        [RequireHttps]
        [HttpPost]
        public ActionResult PluralsightUpdater(string theUserID, string theUserName)
        {
            //update log with start time
            PluralsightUpdateLog theRecord = new PluralsightUpdateLog();
            theRecord.UserID = theUserID;
            theRecord.UserName = theUserName;
            DateTime theStartTime = DateTime.Now;
            theRecord.StartTime = theStartTime;
            db.PluralsightUpdateLogs.Add(theRecord);
            db.SaveChanges();

            WebRequest psRequest = HttpWebRequest.Create("https://api.pluralsight.com/api-v0.9/courses");
            List<ProdPluralsightData> theList = new List<ProdPluralsightData>();
            int courseCount = 0;
            using (WebResponse csv = psRequest.GetResponse())
            {
                Stream theStream = csv.GetResponseStream();
                StreamReader theStreamReader = new StreamReader(theStream);

                string line = null;

                while ((line = theStreamReader.ReadLine()) != null)
                {
                    string[] tempArray = new Regex("," + "(?=(?:[^\"]*\"[^\"]*\")*(?!([^\"]*\")))").Split(line);
                    if (tempArray.Length == 7)
                    {
                        if ((tempArray[0].Equals("CourseID", StringComparison.OrdinalIgnoreCase) == false)
                            && tempArray[6].Equals("yes", StringComparison.OrdinalIgnoreCase) == false)
                        {
                            ProdPluralsightData theCourse = new ProdPluralsightData
                            {
                                CourseId = tempArray[0],
                                CourseTitle = tempArray[1],
                                DurationInSeconds = Convert.ToInt32(tempArray[2]),
                                ReleaseDate = Convert.ToDateTime(tempArray[3]),
                                //if the course description is longer than the varchar size, truncate the string and add ..., else add the line as it is
                                Description = (tempArray[4].Length > 950) ? (tempArray[4].Substring(0, 945) + "...") : tempArray[4],
                                AssessmentStatus = tempArray[5],
                                IsCourseRetired = tempArray[6]
                            };
                            theList.Add(theCourse);
                            ++courseCount;
                        }

                    }
                    else
                    {
                        ViewBag.Message = "Error with one or more lines in CSV file!";
                    }
                }

            }

            db.Database.ExecuteSqlCommand("TRUNCATE TABLE ProdPluralsightData");
            foreach (ProdPluralsightData item in theList)
            {
                try
                {
                    db.ProdPluralsightDatas.Add(item);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            ViewBag.Message = "There are now " + Convert.ToString(courseCount) + " Pluralsight courses available.";
            // reset the values for the current line and the totalLines.

            //update log with end time
            DateTime theEndTime = DateTime.Now;
            foreach (var item in db.PluralsightUpdateLogs)
            {
                if (item.UserID == theUserID && item.EndTime == null)
                {
                    item.EndTime = theEndTime;
                }
            }
            db.SaveChanges();

            return View(theList);
        }

        //Json result for progress.
        public JsonResult UpdateProgress(int totalLines)
        {
            Dictionary<string, double> dict = new Dictionary<string, double>();
            int currentLine = db.ProdPluralsightDatas.Count();
            dict.Add("Status", 0.0);
            dict.Add("LinesRead", totalLines);
            dict.Add("LinesSaved", currentLine);
            return Json(dict, JsonRequestBehavior.AllowGet);
        }

        [RequireHttps]
        public ActionResult About()
        {
            return View();
        }

        public ActionResult TermsOfService()
        {
            return View();
        }

        [RequireHttps]
        [HttpPost]
        public ActionResult ClearSearchHistory(string theUserID)
        {
            foreach (var item in db.SkillifySearches)
            {
                if (item.UserID == theUserID)
                {
                    item.UserID = "Not Logged In";
                }
            }
            db.SaveChanges();

            ViewBag.Message = theUserID;


            return View();
        }

        [Authorize(Roles = "Admin")]
        [RequireHttps]
        [HttpPost]
        public ActionResult PluralsightUpdateLogExplorer(string theUserID, string theUserName)
        {
            return View(db.PluralsightUpdateLogs.ToList());
        }
    }
}