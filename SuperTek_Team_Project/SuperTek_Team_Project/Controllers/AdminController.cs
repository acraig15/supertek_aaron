﻿using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuperTek_Team_Project.Controllers
{
    
    [Authorize(Roles = "Admin")]
    [RequireHttps]
    public class AdminController : Controller
    {
        //private UnitOfWork db = new UnitOfWork(new SkillifyContext());
        SkillifyContext db = new SkillifyContext();

        // GET: Admin
        public ActionResult Index(string userName)
        {
            ViewBag.UserName = userName;
            ViewBag.CourseCount = db.ProdPluralsightDatas.Count();
            return View();
        }
    }
}