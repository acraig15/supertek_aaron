﻿/*Skillify DB up script*/
CREATE TABLE SkillifyUsers (
	UserID nvarchar(128) NOT NULL,
	FirstName nvarchar(255),
	LastName nvarchar(255),
	Username nvarchar(255),
	Age int,
	Birthday DATETIME,
	ASPIdentityID nvarchar(128),
	
	PRIMARY KEY (UserID)
);

CREATE TABLE SkillifySearches (
	SearchID int NOT NULL Identity,
	UserID nvarchar(128) NOT NULL,
	Query nvarchar(255) NOT NULL,
	Time DateTime NOT NULL,
	SkillLevel nvarchar(32) NOT NULL,
	Type nvarchar(32) NOT NULL,

	PRIMARY KEY (SearchID),
	FOREIGN KEY (UserID) REFERENCES SkillifyUsers(UserID)
);

CREATE TABLE SkillifyResources (
	ResourceID int NOT NULL IDENTITY,
	SearchID int NOT NULL,
	Title nvarchar(255) NOT NULL,
	Description nvarchar(3000) NOT NULL,
	Photo nvarchar(255) NOT NULL,
	SkillLevel nvarchar(32) NOT NULL,
	Url nvarchar(255) NOT NULL,
	Source nvarchar(100) NOT NULL,
	Type nvarchar(100) NOT NULL,
	IsDone bit,
	SkillifyRating float NOT NULL,
	UserRating int,
	isPaid bit NOT NULL,
	Duration float,


	PRIMARY KEY (ResourceID),
	FOREIGN KEY (SearchID) REFERENCES SkillifySearches(SearchID)
);

CREATE TABLE SkillifyCurriculum (
	CurriculumID int NOT NULL IDENTITY,
	SkillifyUserID nvarchar(128) NOT NULL,
	AppCurID int,
	Title nvarchar(128) NOT NULL,
	SkillifyResources int,
	DateCreated DateTime NOT NULL,
	DateFinished DateTime,
	IsCreatorFavorite bit NOT NULL,
	IsPublic bit NOT NULL,
	Approved nvarchar(128),
	UsersRating int,
	IsDone bit

	PRIMARY KEY (CurriculumID),
	FOREIGN KEY (SkillifyUserID) REFERENCES SkillifyUsers(UserID),
	FOREIGN KEY (SkillifyResources) REFERENCES SkillifyResources(ResourceID)
);

CREATE TABLE ResourceToCurriculum(
	ID int NOT NULL IDENTITY,
	CurriculumID int,
	ResourceID int, 
	AppCurID int

	PRIMARY KEY (ID)
);

CREATE TABLE ApprovedCurriculum(
	ID int NOT NULL IDENTITY, 
	Title nvarchar (128),
	UserID nvarchar (128),
	SkillifyCurriculums int

	PRIMARY KEY(ID),
	FOREIGN KEY(UserID) REFERENCES SkillifyUsers(UserID),
	FOREIGN KEY(SkillifyCurriculums) REFERENCES SkillifyCurriculum(CurriculumID)

);

CREATE TABLE CurToAppCur(
	ID int NOT NULL IDENTITY,
	AppCurID int,
	CurID int

	PRIMARY KEY (ID)
);

CREATE TABLE ProdPluralsightData
(
   CourseId          VARCHAR(255) NOT NULL PRIMARY KEY
  ,CourseTitle       VARCHAR(255) NOT NULL
  ,DurationInSeconds INTEGER  NOT NULL
  ,ReleaseDate       DATE  NOT NULL
  ,Description       VARCHAR(1000)
  ,AssessmentStatus  VARCHAR(20) NOT NULL
  ,IsCourseRetired   VARCHAR(20) NOT NULL
);

CREATE TABLE PluralsightUpdateLog(
	ID int IDENTITY (0,1),
	UserID nvarchar (128),
	UserName nvarchar (256),
	StartTime datetime,
	EndTime datetime

	CONSTRAINT [PK_dbo.PluralsightUpdateLog] PRIMARY KEY CLUSTERED (ID ASC)
);


--The user to save results to if nobody is logged in. -->
INSERT INTO SkillifyUsers VALUES (
	'Not Logged In', 'NULL', 'NULL', 'NULL', -1, -1, 'Not Logged In'
);

--scripts to enable approved curriculum functionality
INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'CS Basics(Intro)', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Computer Organization/Architecture', GETDATE(), 0, 0, 'Not Logged In');
INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Data Structures', GETDATE(), 0, 0, 'Not Logged In');
INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Algorithms', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Operating Systems', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Network Administration', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Database Management', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Security', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Ethics', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Web Development', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Programming Languages', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Elective(1)', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Elective(2)', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.SkillifyCurriculum(AppCurID, Title, DateCreated, IsPublic, IsCreatorFavorite, SkillifyUserID)
VALUES(1, 'Elective(3)', GETDATE(), 0, 0, 'Not Logged In');

INSERT INTO dbo.ApprovedCurriculum(Title)
Values('Computer Science'); 

INSERT INTO dbo.CurToAppCur(AppCurID, CurID)
SELECT AppCurID, CurriculumID 
FROM dbo.SkillifyCurriculum as a
WHERE a.AppCurID = 1;




