﻿--Skillify Created Tables -->
DROP TABLE SkillifyCurriculum;
DROP TABLE SkillifyResources;
DROP TABLE SkillifySearches;
DROP TABLE SkillifyUsers;
DROP TABLE ResourceToCurriculum;
DROP TABLE ApprovedCurriculum;
DROP TABLE CurToAppCur;
DROP TABLE ProdPluralsightData;
DROP TABLE PluralsightUpdateLog;

--Identity Tables -->
DROP TABLE [dbo].[AspNetUserRoles];
DROP TABLE [dbo].[AspNetRoles];
DROP TABLE [dbo].[AspNetUserClaims];
DROP TABLE [dbo].[AspNetUserLogins];
DROP TABLE [dbo].[AspNetUsers];