namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using SuperTek_Team_Project.Models;
    using SuperTek_Team_Project.Models.Skillify;

    public partial class SkillifyContext : DbContext
    {
        public SkillifyContext()
            : base("name=SkillifyContext")
        {
        }

        public virtual DbSet<ApprovedCurriculum> ApprovedCurriculums { get; set; }
        public virtual DbSet<ProdPluralsightData> ProdPluralsightDatas { get; set; }
        public virtual DbSet<ResourceToCurriculum> ResourceToCurriculums { get; set; }
        public virtual DbSet<SkillifyCurriculum> SkillifyCurriculums { get; set; }
        public virtual DbSet<SkillifyResource> SkillifyResources { get; set; }
        public virtual DbSet<SkillifySearch> SkillifySearches { get; set; }
        public virtual DbSet<SkillifyUser> SkillifyUsers { get; set; }
        public virtual DbSet<CurToAppCur> CurriculumToApprovedCurriculum { get; set; }
        public virtual DbSet<PluralsightUpdateLog> PluralsightUpdateLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.CourseId)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.CourseTitle)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.AssessmentStatus)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.IsCourseRetired)
                .IsUnicode(false);

            modelBuilder.Entity<SkillifyCurriculum>()
                .HasMany(e => e.ApprovedCurriculums)
                .WithOptional(e => e.SkillifyCurriculum)
                .HasForeignKey(e => e.SkillifyCurriculums);

            modelBuilder.Entity<SkillifyResource>()
                .HasMany(e => e.SkillifyCurriculums)
                .WithOptional(e => e.SkillifyResource)
                .HasForeignKey(e => e.SkillifyResources);

            modelBuilder.Entity<SkillifySearch>()
                .HasMany(e => e.SkillifyResources)
                .WithRequired(e => e.SkillifySearch)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkillifyUser>()
                .HasMany(e => e.SkillifyCurriculums)
                .WithRequired(e => e.SkillifyUser)
                .HasForeignKey(e => e.SkillifyUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkillifyUser>()
                .HasMany(e => e.SkillifySearches)
                .WithRequired(e => e.SkillifyUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PluralsightUpdateLog>()
                .Property(e => e.ID)
                .IsRequired();

            modelBuilder.Entity<PluralsightUpdateLog>()
                .Property(e => e.UserID)
                .IsOptional();

            modelBuilder.Entity<PluralsightUpdateLog>()
                .Property(e => e.StartTime)
                .IsOptional();

            modelBuilder.Entity<PluralsightUpdateLog>()
                .Property(e => e.EndTime)
                .IsOptional();
        }
    }
}
