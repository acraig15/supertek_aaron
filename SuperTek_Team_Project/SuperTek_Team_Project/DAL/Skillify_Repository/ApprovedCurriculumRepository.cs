﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.Models.Skillify;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public class ApprovedCurriculumRepository : Repository<ApprovedCurriculum>, IApprovedCurriculumRepository
    {

        public ApprovedCurriculumRepository(SkillifyContext db) : base(db)
        {
            
        }
        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }

        public List<SkillifyCurriculum> AppCurToUser(string id)
        {
            var query = (from s in SkillifyContext.SkillifyCurriculums
                         where s.SkillifyUserID == id
                         where s.Approved != null
                         select s).ToList();

            return query; 
        }

        public void DeleteUserAppCur(string id)
        {
            var query = (from s in SkillifyContext.SkillifyCurriculums
                         where s.SkillifyUserID == id
                         where s.Approved != null
                         select s).ToList();

            foreach(var item in query)
            {
                SkillifyContext.SkillifyCurriculums.Remove(item);
                SkillifyContext.SaveChanges();
            }
        }

        //get list of curriculums within the CS app Cur List
        public ApprovedCurriculumViewModel GetCSAppCurList()
        {
            ApprovedCurriculumViewModel model = new ApprovedCurriculumViewModel();
            model.Title = getTitle();
            var query =
               (from s in SkillifyContext.SkillifyCurriculums
                join a in SkillifyContext.CurriculumToApprovedCurriculum
                on s.CurriculumID equals a.CurID
                where a.AppCurID == 1 select s.Title).ToList();

            List<String> titles = new List<String>();
            foreach (var item in query){

                string curTitle = item;
                titles.Add(curTitle);
            }
            model.CurriculumTitles = titles;
            return model;     
        }

        //get title for the approved curriculum view model 
        public string getTitle()
        {
           string title = SkillifyContext.ApprovedCurriculums.Find(1).Title; 

            return title;

           
        }

        public List<SkillifyCurriculum> ListToAdd(string id)
        {
            List<SkillifyCurriculum> list = new List<SkillifyCurriculum>();


            var app = (from a in SkillifyContext.ApprovedCurriculums
                       where a.ID == 1
                       select a);

            ApprovedCurriculum approved = new ApprovedCurriculum();
            foreach (var item in app){
                approved.ID = item.ID;
                approved.Title = item.Title;
                approved.UserID = id;
            }
                
            var query = (from s in SkillifyContext.SkillifyCurriculums
                         join a in SkillifyContext.CurriculumToApprovedCurriculum
                         on s.CurriculumID equals a.CurID
                         where a.AppCurID == 1
                         select s).ToList();


            foreach(var item in query){
                SkillifyCurriculum cur = new SkillifyCurriculum();
                cur.DateCreated = item.DateCreated;
                cur.SkillifyUserID = id;
                cur.Approved = "A";
                cur.IsCreatorFavorite = item.IsCreatorFavorite;
                cur.Title = item.Title;
                cur.IsPublic = item.IsPublic;
                list.Add(cur);
                SkillifyContext.SkillifyCurriculums.Add(cur);
                SkillifyContext.SaveChanges();
            }

            return list;
        }

    }








}