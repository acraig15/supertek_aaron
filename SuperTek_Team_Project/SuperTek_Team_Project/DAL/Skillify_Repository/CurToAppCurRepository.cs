﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperTek_Team_Project.Models.Skillify;
using SuperTek_Team_Project.DAL.Skillify_Repository.Skillify_Interfaces;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public class CurToAppCurRepository: Repository<CurToAppCur>, ICurToAppCurRepository
    {
        public CurToAppCurRepository(SkillifyContext db) : base(db)
        {

        }
        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }
    }
}
