﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;
using System.Linq;
using System.Collections;
using System.Linq.Expressions;

namespace SuperTek_Team_Project.Skillify_Repository
{
    public class ResourceRepository : Repository<SkillifyResource>, IResourceRepository
    {
        
        public ResourceRepository(SkillifyContext db) :base(db)
        {
           
        }
        
        public List<SkillifyResource> ResToCurList(int? id)
        {

            List<SkillifyResource> resources = new List<SkillifyResource>();
            var query =
                (from s in SkillifyContext.ResourceToCurriculums
                 join r in SkillifyContext.SkillifyResources
                 on s.ResourceID equals r.ResourceID
                 where s.CurriculumID == id
                 select new
                 {
                     ResourceId = r.ResourceID,
                     Description = r.Description,
                     IsPaid = r.isPaid,
                     IsDone = r.IsDone,
                     Photo = r.Photo,
                     Duration = r.Duration,
                     Url = r.Url,
                     Type = r.Type,
                     UserRating = r.UserRating,
                     SkillifyRating = r.SkillifyRating,
                     Title = r.Title,
                     SkillLevel = r.SkillLevel,
                     Source = r.Source
                 }).ToList();

            foreach (var item in query)
            {
                SkillifyResource skillifyResource = new SkillifyResource()
                {
                    ResourceID = item.ResourceId,
                    Description = item.Description,
                    isPaid = item.IsPaid,
                    IsDone = item.IsDone,
                    Photo = item.Photo,
                    Duration = item.Duration,
                    Url = item.Url,
                    Type = item.Type,
                    UserRating = item.UserRating,
                    SkillifyRating = item.SkillifyRating,
                    Title = item.Title,
                    SkillLevel = item.SkillLevel,
                    Source = item.Source
                };
                resources.Add(skillifyResource);
            }
            return resources;
        }

        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }
    }

}