﻿using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;
using System.Data.Entity;

namespace SuperTek_Team_Project.Skillify_Repository
{
    public class UserRepository : Repository<SkillifyUser>, IUserRepository
    {
        private DbSet<SkillifyUser> db = new SkillifyContext().SkillifyUsers;

        public UserRepository(DbContext context) : base(context)
        {

        }
        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }

        public SkillifyUser GetUserByIdentityID(string id) => db.Find(id);
    }
}
