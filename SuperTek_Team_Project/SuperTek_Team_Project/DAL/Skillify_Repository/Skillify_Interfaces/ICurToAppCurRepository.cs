﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperTek_Team_Project.Models.Skillify;

namespace SuperTek_Team_Project.DAL.Skillify_Repository.Skillify_Interfaces
{
   public interface ICurToAppCurRepository: IRepository<CurToAppCur>
    {

    }
}
