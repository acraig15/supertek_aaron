﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public interface ICurriculumRepository: IRepository<SkillifyCurriculum>
    {
       
        List<SearchHistoryModel> SearchHistory(string userID);
        List<SkillifyCurriculum> CurrToUser(string userID);
    }
}
