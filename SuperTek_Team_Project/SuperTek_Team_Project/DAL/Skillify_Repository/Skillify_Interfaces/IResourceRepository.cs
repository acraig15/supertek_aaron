﻿using System.Collections.Generic;
using SuperTek_Team_Project.Models;



namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public interface IResourceRepository : IRepository<SkillifyResource>
    {

        List<SkillifyResource> ResToCurList(int? id);
    }
}