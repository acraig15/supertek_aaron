﻿using SuperTek_Team_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public interface ISearchRepository : IRepository<SkillifySearch>
    {
        IList<SkillifySearch> GetMostRecentSearches(int amount);
        IList<SkillifySearch> GetMostPopularSearches(int amount);
    }
}
