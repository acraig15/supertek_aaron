﻿using SuperTek_Team_Project.Models.Skillify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.ViewModels;
using SuperTek_Team_Project.Models;

    namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public interface IApprovedCurriculumRepository : IRepository<ApprovedCurriculum>
    {
        ApprovedCurriculumViewModel GetCSAppCurList();
        String getTitle();
        List<SkillifyCurriculum> ListToAdd(string id);
        List<SkillifyCurriculum> AppCurToUser(string id);
        void DeleteUserAppCur(string id);
    }
}