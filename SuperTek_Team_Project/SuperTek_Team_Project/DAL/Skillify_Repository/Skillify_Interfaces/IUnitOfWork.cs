﻿using SuperTek_Team_Project.DAL.Skillify_Repository.Skillify_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    interface IUnitOfWork : IDisposable
    {
        IResourceRepository Resource { get; }
        ICurriculumRepository Curriculum { get; }
        IResToCurRepository ResToCur { get; }
        ISearchRepository Search { get; }
        IUserRepository User { get; }
        IApprovedCurriculumRepository AppCur{get;}
        ICurToAppCurRepository CurToApp { get; }
        void Save();
    }
}
