﻿using SuperTek_Team_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public interface IUserRepository : IRepository<SkillifyUser>
    {
        SkillifyUser GetUserByIdentityID(string id);
    }
}