﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.Skillify_Repository;
using SuperTek_Team_Project.DAL.Skillify_Repository.Skillify_Interfaces;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly SkillifyContext context;

        public UnitOfWork(SkillifyContext _context)
        {
            context = _context;
            Resource = new ResourceRepository(context);
            Curriculum = new CurriculumRepository(context);
            ResToCur = new ResToCurRepository(context);
            Search = new SearchRepository(context);
            User = new UserRepository(context);
            AppCur = new ApprovedCurriculumRepository(context);
           CurToApp = new CurToAppCurRepository(context);
        }

        public IResourceRepository Resource { get; private set; }
        public ICurriculumRepository Curriculum { get; private set; }
        public IResToCurRepository ResToCur { get; private set; }
        public ISearchRepository Search { get; private set; }
        public IUserRepository User { get; private set; }
        public IApprovedCurriculumRepository AppCur { get; private set; }
        public ICurToAppCurRepository CurToApp { get; private set; }



        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}