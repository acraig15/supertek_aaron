﻿using SuperTek_Team_Project.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    /// <summary>
    /// Pluralsight Update Log Repository Class
    /// </summary>
    public class PluralsightUpdateLogRepository : IRepository<PluralsightUpdateLog>
    {
        private SkillifyContext context;
        public PluralsightUpdateLogRepository(SkillifyContext context)
        {
            this.context = context;
        }
        /// <summary>
        /// Get all of the entries in the log.
        /// </summary>
        /// <returns>An enumerable set of log entries.</returns>
        public IEnumerable<PluralsightUpdateLog> GetAll()
        {
            return context.PluralsightUpdateLogs.ToList();
        }

        public PluralsightUpdateLog Get(int logID)
        {
            return context.PluralsightUpdateLogs.Find(logID);
        }

        //public IEnumerable<PluralsightUpdateLog> Find(Expression<Func<PluralsightUpdateLog, bool>> predicate)
        //{
        //    if (predicate == null)
        //    {
        //        throw new ArgumentNullException(nameof(predicate));
        //    }

        //    return context.PluralsightUpdateLogs.Where(predicate).ToList();
        //}
        public void Add(PluralsightUpdateLog log)
        {
            context.PluralsightUpdateLogs.Add(log);
            //context.SaveChanges();            
        }

        IEnumerable IRepository<PluralsightUpdateLog>.Find(Expression<Func<PluralsightUpdateLog, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return context.PluralsightUpdateLogs.Where(predicate).ToList();
        }

        public void Delete(PluralsightUpdateLog entity)
        {
            context.Set<PluralsightUpdateLog>().Add(entity);
        }

        public void Update(object entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}