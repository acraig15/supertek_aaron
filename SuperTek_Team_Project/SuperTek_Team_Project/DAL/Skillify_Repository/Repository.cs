﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext dbcontext; 

        public Repository(DbContext context)
        {
            dbcontext = context;
        }

        public void Add(TEntity entity)
        {
            dbcontext.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            dbcontext.Set<TEntity>().Add(entity);
        }

        public IEnumerable Find(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return dbcontext.Set<TEntity>().Where(predicate);
        }

        public TEntity Get(int id)
        {
            return dbcontext.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return dbcontext.Set<TEntity>().ToList();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Update(object entity)
        {
            dbcontext.Entry(entity).State = EntityState.Modified;
        }
    }
}