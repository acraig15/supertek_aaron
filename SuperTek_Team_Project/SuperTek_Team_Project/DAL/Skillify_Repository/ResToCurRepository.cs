﻿using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;

namespace SuperTek_Team_Project.DAL.Skillify_Repository
{
    public class ResToCurRepository : Repository<ResourceToCurriculum>, IResToCurRepository
    {
        public ResToCurRepository(DbContext context) : base(context)
        {
        }
        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }
    }
}