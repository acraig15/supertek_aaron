﻿using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.Skillify_Repository
{
    public class CurriculumRepository : Repository<SkillifyCurriculum>, ICurriculumRepository
    {
        public CurriculumRepository(DbContext context) : base(context)
        {
        }

        //grab a list of search history by a given user
        public List<SearchHistoryModel> SearchHistory(string userID)
        {
            List<SearchHistoryModel> history = new List<SearchHistoryModel>();
            //var query =
            //       (from s in SkillifyContext.SkillifySearches
            //        join r in SkillifyContext.SkillifyResources
            //        on s.SearchID equals r.SearchID
            //        where s.SkillifyUser.UserID == userID
            //        orderby s.Time descending

            //        select new
            //        {
            //            ResourceID = r.ResourceID,
            //            query = s.Query,
            //            date = s.Time,
            //            description = r.Description,
            //            url = r.Url,
            //            title = r.Title
            //        }).ToList();

            // Same as above with fluent syntax.
            var query = SkillifyContext
                        .SkillifySearches
                        .Join(SkillifyContext.SkillifyResources,
                                              s => s.SearchID, r => r.SearchID, (s, r) => new { s, r })
                        .Where(n => n.s.UserID == userID)
                        .OrderByDescending(n => n.s.Time)
                        .Select(z => new { ResourceID = z.r.ResourceID, query = z.s.Query, date = z.s.Time, description = z.r.Description, url = z.r.Url, title = z.r.Title }).ToList();

            //add each item found into the list 
            foreach (var item in query)
            {
                SearchHistoryModel sh = new SearchHistoryModel
                {
                    ResourceID = item.ResourceID,
                    search = item.query,
                    date = item.date,
                    description = item.description,
                    title = item.title,
                    url = item.url
                };
                history.Add(sh);

            }
            return history;
        }

        //returns a list of custom curriculums by a given user
        public List<SkillifyCurriculum> CurrToUser(string userID)
        {
         
           var list = from s in SkillifyContext.SkillifyCurriculums.Include(s => s.SkillifyResource).Include(s => s.SkillifyUser)
                                      where s.SkillifyUserID == userID
                                      where s.Approved == null
                                      select s;

            return list.ToList();
        }

        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }
    }
}