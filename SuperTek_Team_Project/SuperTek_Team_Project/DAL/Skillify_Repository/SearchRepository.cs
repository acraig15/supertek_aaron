﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Models;

namespace SuperTek_Team_Project.Skillify_Repository
{
    public class SearchRepository : Repository<SkillifySearch>, ISearchRepository
    {
        public SearchRepository(DbContext context) : base(context)
        {
        }

        public IList<SkillifySearch> GetMostRecentSearches(int amount)
        {
            var searches = SkillifyContext.SkillifySearches.ToArray();

            IList<SkillifySearch> myList = new List<SkillifySearch>();
            for(var i = searches.Count() - 1; i > 0; --i)
            {
                if (searches.Count() - i > amount)
                    break;
                myList.Add(searches[i]);
            }
            return myList;
        }

        public IList<SkillifySearch> GetMostPopularSearches(int amount)
        {
            var searches = (from x in SkillifyContext.SkillifySearches
                            where x.UserID != "Not Logged In"
                            select x).GroupBy(e => e.Query).OrderByDescending(e => e.Count()).ToArray();

            IList<SkillifySearch> myList = new List<SkillifySearch>();

            for (int i = 0; i < amount; ++i)
            {
                if (i >= searches.Count()) //If there's not enough info in the database
                    break;
                myList.Add(searches[i].FirstOrDefault());
            }

            //Fill it up the rest of the way, if it's not all the way full
            if(myList.Count() < amount)
            {
                for(int i = myList.Count(); i < amount; ++i)
                {
                    SkillifySearch temp = new SkillifySearch();
                    temp.Query = "Coming Soon!";
                    myList.Add(temp);
                }
            }

            return myList;
        }
        public SkillifyContext SkillifyContext
        {
            get { return dbcontext as SkillifyContext; }
        }
    }
}