﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class KhanAPI
    {
        public static IList<SearchResult> KhanAcad(string query, string level)
        {
            //making the string usable by khan academy api if there is spaces within the query
            query = query.TrimEnd('+');
            query = query.Replace("+", "-");

            //Debug.WriteLine(query);

            string source = "https://khanacademy.org/api/v1/topic/" + query;

            //make the web request
            WebRequest request = WebRequest.Create(source);

            //get the response from API
            WebResponse res = request.GetResponse();

            //stream data from server 
            Stream stream = res.GetResponseStream();

            //see what we got here 
            string apiTalk = new StreamReader(stream).ReadToEnd();

            //serialize the string into a JSON object
            var serialize = new JavaScriptSerializer();
            var data = serialize.DeserializeObject(apiTalk);

            //convert JSON into a C# object
            JObject finishedString = JObject.Parse(apiTalk);
            IList<JToken> jsonObject = finishedString["children"].Children().ToList();
            //IList<Models.KhanTopicSearch.Child> searchResults = new List<Models.KhanTopicSearch.Child>();
            IList<SearchResult> searchResults = new List<SearchResult>();

            //class where main important info is 
            //adding each child to the list 
            int maxResults = 5;
            foreach (var result in jsonObject)
            {
                KhanTopicSearch.Child found = result.ToObject<KhanTopicSearch.Child>();
                string skillLevel;
                if (found.description.ToUpperInvariant().Contains("INTERMEDIATE"))
                {
                    skillLevel = "intermediate";
                }
                else if (found.description.ToUpperInvariant().Contains("ADVANCED"))
                {
                    skillLevel = "advanced";
                }
                else
                    skillLevel = "beginner";

                SearchResult temp = new SearchResult
                {
                    Title = found.title,
                    Description = found.description,
                    Image = found.icon_large,
                    Url = found.url,
                    SkillLevel = skillLevel,
                    Source = "Khan Academy",
                    Type = "Course",
                    ResourceLength = 10800f
                };

                if (temp.SkillLevel == level)
                {
                    searchResults.Add(temp);
                }
                //Only save the first few found
                if (searchResults.Count >= maxResults)
                    break;
            }

            //closing streams/requests 
            res.Close();
            stream.Close();

            return searchResults;
        }
    }
}
