﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Udemy_Models;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class UdemyAPI
    {
        public static IList<SearchResult> CourseListSearch(string query, string level, string price)
        {
            //Get secret API keys
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["UdemyKey"];
            int results = 10;
            //Parse level to Udemy friendly terms
            if (level == "advanced")
                level = "expert";
            //Paid or free 
            string ordering;
            if (price == "free")
            {
                price = "price-free";
                ordering = "price-low-to-high";
                //Increase the amount of resulsts to get a good amount of free ones
                results *= 3;
            }
            else if (price == "paid")
            {
                price = "price-paid";
                ordering = "highest-rated";
            }
            else
            {
                price = "";
                ordering = "relevance";
            }

            //Send the get request to Udemy
            WebRequest req = WebRequest.Create("https://www.udemy.com/api-2.0/courses/?instructional_level=" + level + "&fields[course]=@all,page=1&page_size=" + results + "&search=" + query + "&sort=" + ordering + "&price=" + price + "&language = en");
            req.Headers.Add("Authorization", key);
            WebResponse rep = req.GetResponse();
            Stream dataStream = rep.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            //Close streams
            reader.Close();
            rep.Close();

            //Convert from json to C#
            JObject finishedString = JObject.Parse(responseFromServer);

            IList<JToken> myJsonObject = finishedString["results"].Children().ToList();

            IList<SearchResult> searchResults = new List<SearchResult>(); //The final object to be displayed

            if (price == "Free")
            {
                for (int i = 0; i < myJsonObject.Count; ++i)
                {
                    //Filter out the ones that aren't free if necessary
                    if (myJsonObject[i].Children().ToList()[4].First.ToString() == "False")
                    {
                        SearchResult found = new SearchResult
                        {
                            Image = myJsonObject[i].Children().ToList()[10].First.ToString(),
                            Description = myJsonObject[i].Children().ToList()[12].First.ToString().Substring(0, 300),
                            Url = "https://www.udemy.com" + myJsonObject[i].Children().ToList()[3].First.ToString(),
                            Source = "Udemy",
                            Type = "Course",
                            SkillLevel = level,
                            Title = myJsonObject[i].Children().ToList()[2].First.ToString(),
                            VideoID = myJsonObject[i].Children().ToList()[1].First.ToString()
                        };
                        float.TryParse(myJsonObject[i].Children().ToList()[66].First.ToString(), out float temp);
                        found.ResourceLength = temp * 60;
                        searchResults.Add(found);
                    }
                }
            }
            else
            {
                for (int i = 0; i < myJsonObject.Count; ++i)
                {
                //Filter out the ones that aren't free if necessary
                    SearchResult found = new SearchResult
                    {
                        Image = myJsonObject[i].Children().ToList()[10].First.ToString(),
                        Description = myJsonObject[i].Children().ToList()[12].First.ToString().Substring(0, 300),
                        Url = "https://www.udemy.com" + myJsonObject[i].Children().ToList()[3].First.ToString(),
                        Source = "Udemy",
                        Type = "Course",
                        SkillLevel = level,
                        Title = myJsonObject[i].Children().ToList()[2].First.ToString(),
                        VideoID = myJsonObject[i].Children().ToList()[1].First.ToString()
                    };
                    float.TryParse(myJsonObject[i].Children().ToList()[66].First.ToString(), out float temp);
                    found.ResourceLength = temp * 60;
                    searchResults.Add(found);
                }
            }

            return searchResults;
        }

        public static UdemyCourseDetails.RootObject GetCourseDetails(int VideoID)
        {
            //Get secret API keys
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["UdemyKey"];

            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.udemy.com/api-2.0/courses/" + VideoID + "?fields[course]=num_subscribers,num_reviews,rating,estimated_content_length");
            req.Headers.Add("Authorization", key);
            WebResponse rep = req.GetResponse();
            Stream dataStream = rep.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            //Close streams
            reader.Close();
            rep.Close();

            JObject FinishedString = JObject.Parse(responseFromServer);
            //Map it to the model
            return FinishedString.ToObject<UdemyCourseDetails.RootObject>();
        }
    }
}