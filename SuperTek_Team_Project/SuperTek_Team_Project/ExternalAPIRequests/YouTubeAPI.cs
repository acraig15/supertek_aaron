﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using SuperTek_Team_Project.ViewModels;
using YouTubeVideoStats;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class YouTubeAPI
    {
        /// <summary>
        /// Sends a formatted request to an external website.
        /// </summary>
        /// <param name="request">The formatted, ready to send WebRequest.</param>
        /// <returns>A string with the final returned result.</returns>
        public static string QueryYT(WebRequest request)
        {
            WebResponse rep = request.GetResponse();
            Stream dataStream = rep.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string result = reader.ReadToEnd();

            //Close streams
            rep.Close();
            reader.Close();
            dataStream.Close();

            return (result);
        }

        /// <summary>
        /// The basic YouTube search, sorted by relevance.
        /// </summary>
        /// <param name="query">The term to search on.</param>
        /// <param name="level">The skill level to look for.</param>
        /// <returns>A list of SearchResults filled with each video's data.</returns>
        public static IList<SearchResult> Search(string query, string level)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            int maxResults = 10; //The # of results to get back
            query = level + " " + query; //Add the skill level to the query itself

            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=" + maxResults + "&order=relevance&q=" + query + "&key=" + apiKey); //Sends the get to YouTube API

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));

            IList<JToken> myJsonObject = finishedString["items"].Children().ToList(); //Grab all data from items and put it in json object

            IList<SearchResult> searchResults = new List<SearchResult>(); //The default class where most of the info is at

            //Add all the results, and filter out ones that shouldn't be there
            foreach (JToken result in myJsonObject)
            {
                YouTube_Models.Result YouTubeData = result.ToObject<YouTube_Models.Result>();
                SuperTek_Team_Project.ViewModels.SearchResult found = new SuperTek_Team_Project.ViewModels.SearchResult();
                //Ignore channels/users
                if (YouTubeData.id.kind != "youtube#channel")
                {
                    if (YouTubeData.id.kind == "youtube#video") //For specific video links
                    {
                        found.Url = "https://www.youtube.com/watch?v=" + YouTubeData.id.videoId;
                        found.VideoID = YouTubeData.id.videoId;
                        found.Type = "Video";
                        var length = VideoStats(YouTubeData.id.videoId).items[0].contentDetails.duration;
                        var duration = System.Xml.XmlConvert.ToTimeSpan(length);
                        found.ResourceLength = (float)duration.TotalSeconds;
                    }
                    else if (YouTubeData.id.kind == "youtube#playlist") //Playlist links
                    {
                        found.Url = "https://www.youtube.com/playlist?list=" + YouTubeData.id.playlistId;
                        found.VideoID = YouTubeData.id.playlistId;
                        found.Type = "Playlist";
                        found.ResourceLength = GetLengthOfPlaylist(YouTubeData.id.playlistId);
                    }
                    //Fill up search result
                    found.Description = YouTubeData.snippet.description;
                    if (found.Description == "")
                        found.Description = "No description could be found for this resource.";
                    found.Image = YouTubeData.snippet.thumbnails.medium.url;
                    found.SkillLevel = level;
                    found.Source = "YouTube";
                    found.Title = YouTubeData.snippet.title;

                    searchResults.Add(found);
                }
            }

            return searchResults; //Return final list
        }
        
        /// <summary>
        /// Retrieve the video statistics, such as length, comments, and more, for one video.
        /// </summary>
        /// <param name="id">The video ID for a YouTube video.</param>
        /// <returns>A YouTubeVideoStats.RootObject model.</returns>
        public static YouTubeVideoStats.RootObject VideoStats(string id)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics&id=" + id + "&key=" + apiKey); //Sends the get to YouTube API

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));
            //Get just the data under item, not root object
            var result = finishedString.ToObject<RootObject>();

            return result;
        }

        /// <summary>
        /// Returns the information about the first video in a playlist.
        /// </summary>
        /// <param name="PlaylistID">The ID of a playlist on YouTube</param>
        /// <returns>A YouTubeSnippet.Snippet model of the first video in a playlist.</returns>
        public static YouTubeSnippet.Snippet GetFirstVideoInPlaylist(string PlaylistID)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + PlaylistID + "&key=" + apiKey);

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));

            var result = finishedString.ToObject<YouTubeSnippet.RootObject>();

            return result.items[0].snippet;
        }

        /// <summary>
        /// Get the length of a playlist in hours.
        /// </summary>
        /// <param name="PlaylistID">The YouTube ID of the playlist to get the length of.</param>
        /// <returns>The length of the playlist in hours, as a float.</returns>
        public static float GetLengthOfPlaylist(string PlaylistID)
        {
            double playlistLength = 0;
            var firstCall = GetNextPlayelistSection(PlaylistID);
            IList<YouTubeSnippet.RootObject> videoObjects = new List<YouTubeSnippet.RootObject>();
            videoObjects.Add(firstCall);
            while(videoObjects[videoObjects.Count - 1].nextPageToken != null)
            {
                videoObjects.Add(GetNextPlayelistSection(PlaylistID, videoObjects[videoObjects.Count - 1].nextPageToken));
            }
            
            foreach(var item in videoObjects)
            {
                for(int i = 0; i < item.items.Count(); ++i)
                {
                    //Convert duration string to int
                    var length = VideoStats(item.items[i].snippet.resourceId.videoId).items[0].contentDetails.duration;
                    var duration = System.Xml.XmlConvert.ToTimeSpan(length);
                    playlistLength += duration.TotalSeconds;
                }
            }
            string aString = playlistLength.ToString();
            float.TryParse(aString, out float time);

            return time;
        }
        /// <summary>
        /// Helper method for getting the max results of a playlist YouTube API call.
        /// </summary>
        /// <param name="PlaylistID">The YouTube playlist ID to look up.</param>
        /// <param name="PageToken">The page of results to get.</param>
        /// <returns>The YouTubeSnippet.RootObject that contains all the individual data about up to 50 videos from that playlist.</returns>
        private static YouTubeSnippet.RootObject GetNextPlayelistSection(string PlaylistID, string PageToken = "")
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            WebRequest req;
            if (PageToken == "")
            {
                req = WebRequest.Create("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=" + PlaylistID + "&key=" + apiKey);
            }
            else
            {
                req = WebRequest.Create("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&pageToken=" + PageToken + "&playlistId=" + PlaylistID + "&key=" + apiKey);
            }
            JObject finishedString = JObject.Parse(QueryYT(req));

            return finishedString.ToObject<YouTubeSnippet.RootObject>();
        }
    }
}