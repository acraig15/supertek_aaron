﻿function beginSearch() {
    var searchQuery = document.getElementById("searchBar").value;
    var level, cost;
    //Set the level the user selected by checking radio buttons
    if (document.getElementById("beg").checked)
        level = "beginner";
    else if (document.getElementById("int").checked)
        level = "intermediate";
    else
        level = "advanced";
    //Get whether it's paid or free
    if (document.getElementById("free").checked)
        cost = "free";
    else if (document.getElementById("paid").checked)
        cost = "paid";
    else
        cost = "both";
    //Safify searchQuery
    var safeQuery = encodeURIComponent(searchQuery);
    //Redirect to method in home controller
    location.href = '/search?query=' + safeQuery + "&level=" + level + "&cost=" + cost;
}

function checkEnter(key) {
    if (key.keyCode === 13)
        beginSearch();
}