﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SuperTek_Team_Project.DAL.Skillify_Repository;
using SuperTek_Team_Project.Controllers;
using SuperTek_Team_Project.Models;
using NUnit.Framework;
using System.Web.Mvc;
using SuperTek_Team_Project.ViewModels;

namespace SuperTekTesting
{
    [TestFixture]
    public class CurriculumMoq
    {
        private List<SkillifyCurriculum> curriculums;
        private List<ResourceToCurriculum> resToCur; 
        private Mock<ICurriculumRepository> mock;
        private Mock<IResToCurRepository> resToCurMock;

        private UserPortalController userPortal;


        [SetUp]
        public void SetUp()
        {

            resToCur = new List<ResourceToCurriculum>
            {
                new ResourceToCurriculum{ID = 1, CurriculumID = 1, ResourceID = 2},
                new ResourceToCurriculum{ID = 2, CurriculumID = 3, ResourceID = 42},
                new ResourceToCurriculum{ID = 3, CurriculumID = 3, ResourceID = 32},
                new ResourceToCurriculum{ID = 4, CurriculumID = 2, ResourceID = 10}
            };

            resToCurMock = new Mock<IResToCurRepository>();

            resToCurMock.Setup(mr => mr.GetAll())
             .Returns(resToCur);

            curriculums = new List<SkillifyCurriculum>
            {
                new SkillifyCurriculum{CurriculumID = 1, Title = "Curriculum1", SkillifyUserID = new Random().ToString() },
                new SkillifyCurriculum{CurriculumID = 2, Title = "Curriculum2", SkillifyUserID = new Random().ToString() },
                new SkillifyCurriculum{CurriculumID = 3, Title = "Curriculum3", SkillifyUserID = new Random().ToString() },
                new SkillifyCurriculum{CurriculumID = 4, Title = "Curriculum4", SkillifyUserID = new Random().ToString()},
                new SkillifyCurriculum{CurriculumID = 5, Title = "Curriculum5", SkillifyUserID = new Random().ToString() }
            };

            mock = new Mock<ICurriculumRepository>();
            mock.Setup(m => m.GetAll())
               .Returns(curriculums);

            mock.Setup(mr => mr.getByID(
               It.IsAny<int>())).Returns((int i) => curriculums.Where(
                 x => x.CurriculumID == i).Single());

            mock.Setup(d => d.Add(It.IsAny<SkillifyCurriculum>())).Callback<SkillifyCurriculum>((s) => curriculums.Add(s));
        }
        [SetUp]

        //can we return all the curriculums?
        [Test]
        public void TestingReturningAllCurriculums()
        {
            IList<SkillifyCurriculum> skillifyCurriculum = mock.Object.GetAll();

            Assert.IsNotNull(skillifyCurriculum);
            Assert.AreEqual(5, skillifyCurriculum.Count);
        }

        //can we find the curriculum by id? 
        [Test]
        public void FindCurriculumByID()
        {
            SkillifyCurriculum testCur = mock.Object.getByID(4);

            Assert.IsNotNull(testCur);
            Assert.AreEqual("Curriculum4", testCur.Title);
        }

        //can we insert a curriculum?
        [Test]
        public void TestingAddingACurriculum()
        {
            
            SkillifyCurriculum newCur = new SkillifyCurriculum
            {
                CurriculumID = 6,
                Title = "Curriculum6",
                SkillifyUserID = new Random().ToString()
            };
            mock.Object.Add(newCur);
            Assert.AreEqual(mock.Object.GetAll().Count, 6);


        }

        [Test]
       public void EnsureUserPortalIsNotNull()
        {

            userPortal = new UserPortalController(mock.Object, resToCurMock.Object);
            Assert.IsNotNull(userPortal);
           
        }
    }
}*/