﻿using System;
using NUnit.Framework;
using SuperTek_Team_Project.ViewModels;

namespace SuperTekTesting
{
    [TestFixture]
    public class SkillifyTest
    {
        protected void SetUp(){

        }
        [Test]
        //Testing to ensure that the search history model is working properly
        public void SearchHistoryModelValidationTest()
        {
            DateTime currentDate = DateTime.Now;
            var history = new SearchHistoryModel()
            {
                search = "algebra",
                description = "this is a beginning algebra course",
                title = "beginning algebra",
                date = currentDate
            };

            Assert.AreNotEqual("alegebra", history.search);
            Assert.AreEqual("this is a beginning algebra course", history.description);
            Assert.AreEqual("beginning algebra", history.title);
            Assert.AreEqual(currentDate, history.date);
        }

        //testing that a search result has a description of 255 characters
        [Test]
        public void SearchResultModelDescription255Characters()
        {
            var result = new SearchResult();
            result.Description = "this is a test description. This should not be more than 255 characters";

            Assert.That(result.Description.Length, Is.LessThan(255));
        }
        [Test]
        public void SearchHistoryTitleIsNotNull()
        {
            var history = new SearchHistoryModel();
            history.title = "learn html";

            Assert.That(history.title, Is.Not.Null);
        }
        protected void TearDown()
        {

        }
    }
}
