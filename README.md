# README


This is a repository for SuperTek, a team for the CS461/CS462 Senior Project 2018 at Western Oregon University. The main project in this course is to serve as a capstone project for a Bachelor's Degree in Computer Science.

## The contents of this repository includes:
1. Business Documents
    *  All documents/images/models associated with SuperTek Organization
2. XC Running App
    *  An in class project that was used to learn about workflows and the SCRUM process
3. src/SuperTek_Team_Project
    *  Where the code for our team project lives

## SuperTek Members

*   Aaron Craig
*   Jazmin Bembry
*   Luis Loyh


## SuperTek Project

### Skillify- A Skill Learning / Online Degree Course Fusion Website

https://skillify.azurewebsites.net/

The project Supertek has chosen to produce for their senior capstone is a website that allows one to search for couses across the top learning websites: Udemy, Coursera, Edx, Khan Academy, and PluralSight. Also, those deciding to create an account can plan a curriculum of their choice and leave ratings and testimonies for the different courses from the different websites for other users and searchers to be able to gain knowledge from. The website can also recommend courses and curate lists of courses based on ratings and aspirations of the user.



## Vision Statement

For students of all ages and everyday people looking to learn a new skill, the Skill Learning / Online Degree Course Fusion website allows for users to search for top online courses from the leading learning websites, including Udemy, Coursera, Edx, PluralSight, and Khan Academy. The website  allows learners to view and evaluate courses from the various different learning platforms  based on price, rating, single skill or degree, and curate an experience that suits his or her needs. The website will save the user time and energy by allowing them to have all of the best options in one search, versus looking through each website individually. Also, they can see ratings from the various learning websites, as well as ratings and testimonies left by users of the Skill Learning / Online Degree Course Fusion website. In addition, users are able to create an account and save their searches and be updated when new, great courses that align with their educational needs are available.The website allows a user to find and plan a given curriculum that suits his or her needs, whether all of the courses be from one website, or from various websites that are recommended based on their previous searches or interests that he or she selects in a user profile. Unlike websites such as OEDB.com which has a search engine that includes several online learning platforms, the Skill Learning / Online Degree Course Fusion website will have more options in searching on different criteria and access to a more curated experience.

## Getting Started and Rules/Guidelines

The [WorkingWithSuperTek](WorkingWithSuperTek.md ) file contains rules and guidelines about how to get started with the project and contributing/coding standards that will aid you in the process.

## Software Construction Process

For this project, we are following Agile methodologies, specifically Disciplined Agile Delivery process framework. We follow a two-week sprint cycle that involves daily SCRUM meetings, weekly sprint planning sessions, an end of sprint review and retrospective. To contribute to this project, one will need to be involved in this process with the SuperTek team. We also use Visual Studio Team Services to track our processes. You will want to get an account associated with the project in order to stay up to date on schedules and product backlogs.


## Tools, Configuation, and Packages

The [tools](tools.md) file is a list of all the software, libraries, tools, packages, and versions used on this project. Make sure you are using the exacts ones to avoid compatability issues.





